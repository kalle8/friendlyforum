using System;

namespace FriendlyForum.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public User(string name)
        {
            this.Name = name;
            this.Created = DateTime.UtcNow;
            this.Updated = DateTime.UtcNow;
        }
    }
}