using System;

namespace FriendlyForum.Models
{
    public class ForumPost
    {
        public ForumPost(Guid userId, string title, string body)
        {
            UserId = userId;
            Title = title;
            Body = body;
            Created = DateTime.UtcNow;
            Updated = DateTime.UtcNow;
        }
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public void Update(string title, string body)
        {
            this.Title = title;
            this.Body = body;
            this.Updated = DateTime.UtcNow;
        }

        public bool Edited => this.Created != this.Updated;
    }
}