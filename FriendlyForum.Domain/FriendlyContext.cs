using Microsoft.EntityFrameworkCore;

namespace FriendlyForum.Models
{
    public class FriendlyContext : DbContext
    {
        protected FriendlyContext() {}
        public FriendlyContext(DbContextOptions<FriendlyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ForumPost> ForumPosts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ForumPost>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<User>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
        }

    }
}