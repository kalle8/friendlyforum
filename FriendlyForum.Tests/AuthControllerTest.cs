using System.Threading.Tasks;
using FluentAssertions;
using FriendlyForum.Controllers;
using FriendlyForum.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace FriendlyForum.Tests
{
    public class AuthControllerTests : ControllerUnitTestBase
    {
        private ILogger<AuthController> logger;
        public AuthControllerTests() : base()
        {
            this.logger = Mock.Of<ILogger<AuthController>>();
            this.userService.Setup(r => r.AuthenticateAsync(It.IsAny<string>())).ReturnsAsync(new User("Kalle"));
        }

        [Fact]
        public async Task NameAuthWorks()
        {
            var controller = new AuthController(this.userService.Object, this.logger, this.config);

            var result = await controller.AuthenticateAsync("Kalle");

            var okResult = result.Result.Should().BeOfType<OkObjectResult>().Subject;
            var authData = okResult.Value.Should().BeOfType<AuthData>().Subject;
            authData.User.Name.Should().Be("Kalle");
            authData.Token.Should().NotBeNullOrWhiteSpace();

        }

        [Fact]
        public async Task EmptyNameBadRequest()
        {
            var controller = new AuthController(this.userService.Object, this.logger, this.config);
            var result = await controller.AuthenticateAsync("");
            result.Result.Should().BeOfType<BadRequestObjectResult>();
        }

    }
}