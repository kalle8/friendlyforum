using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentAssertions;
using FriendlyForum.Controllers;
using FriendlyForum.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace FriendlyForum.Tests
{
    public class ForumControllerTests : ControllerUnitTestBase
    {
        private ILogger<ForumController> logger;
        public Guid OurPostId = Guid.NewGuid();
        public Guid OurUserId = Guid.NewGuid();

        public ForumController Controller;
        public ForumControllerTests() : base()
        {
            this.logger = Mock.Of<ILogger<ForumController>>();

            this.postService.Setup(r => r.GetPosts())
                .Returns(new List<ForumPost>() { new ForumPost(this.OurUserId, "Test", "Test") });
            this.postService.Setup(r => r.GetPost(It.Is<Guid>(r => r == this.OurPostId)))
                .ReturnsAsync(new ForumPost(this.OurUserId, "Test", "Test") { Id = this.OurPostId });

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
               {
               new Claim(ClaimTypes.Name, this.OurUserId.ToString())
               }));

            this.Controller = new ForumController(this.logger, this.postService.Object);

            this.Controller.ControllerContext = new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
        }

        [Fact]
        public void GetPostsTest()
        {
            var result = this.Controller.GetPosts();

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var posts = okResult.Value.Should().BeAssignableTo<List<ForumPost>>().Subject;
            posts.Count.Should().Be(1);
        }

        [Fact]
        public async Task GetPostTestAsync()
        {
            var result = await this.Controller.GetPost(this.OurPostId);

            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;

            okResult.Value.Should().BeAssignableTo<ForumPost>().Subject.Id.Should().Be(this.OurPostId);
        }

        [Fact]
        public async Task CreatePostAsync()
        {
            var title = "Title";
            var body = "Body";
            this.postService.Setup(r => r.CreatePost(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new ForumPost(Guid.NewGuid(), title, body));

            var result = await this.Controller.CreatePost(new CreatePostDto(title, body));
            var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;

            okResult.StatusCode.Should().Be(201);
        }


        [Fact]
        public async Task CreatePostInvalidModelAsync()
        {
            var title = "Title";
            var body = "";
            this.Controller.ModelState.AddModelError("Title", "Required");

            var result = await this.Controller.CreatePost(new CreatePostDto(title, body));

            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public async Task UpdatePostAsync()
        {
            var title = "Title Changed";
            var body = "Body";
            this.postService.Setup(r => r.UpdatePost(It.Is<Guid>(g => g == this.OurUserId), It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new ForumPost(this.OurUserId, title, body));

            var result = await this.Controller.UpdatePost(this.OurPostId, new CreatePostDto(title, body));


            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var post = okResult.Value.Should().BeAssignableTo<ForumPost>().Subject;
            post.Title.Should().Be(title);
        }

        [Fact]
        public async Task DeletePost()
        {
            var result = await this.Controller.DeletePost(this.OurPostId);

            var okResult = result.Should().BeOfType<OkResult>().Subject;
        }

    }
}