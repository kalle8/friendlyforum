using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using FriendlyForum.Contracts;
using FriendlyForum.Models;
using FriendlyForum.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace FriendlyForum.Tests
{
    public class PostServiceTests
    {

        private ILogger<PostService> logger;

        public PostService ourService;
        public Mock<DbSet<ForumPost>> mockSet;
        public Mock<FriendlyContext> mockContext;
        public PostServiceTests()
        {
            this.logger = Mock.Of<ILogger<PostService>>();

            this.mockSet = new Mock<DbSet<ForumPost>>();

            this.mockContext = new Mock<FriendlyContext>();
            this.mockContext.Setup(m => m.ForumPosts).Returns(mockSet.Object);

            this.ourService = new PostService(logger, this.mockContext.Object);
        }

        [Fact]
        public async Task AddAPost()
        {
            var post = await this.ourService.CreatePost(Guid.NewGuid(), "Test", "Testing the service");

            this.mockSet.Verify(m => m.AddAsync(It.IsAny<ForumPost>(), It.IsAny<CancellationToken>()), Times.Once());
            this.mockContext.Verify(m => m.SaveChangesAsync(CancellationToken.None), Times.Once());

        }

    }
}