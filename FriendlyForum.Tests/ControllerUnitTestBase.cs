using System.Collections.Generic;
using FriendlyForum.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;

namespace FriendlyForum.Tests
{
    public class ControllerUnitTestBase
    {
        public Mock<IUserService> userService;
        public Mock<IPostService> postService;
        public IConfiguration config;
        public ControllerUnitTestBase()
        {
            var inMemorySettings = new Dictionary<string, string> { { "Secret", "At some point this should most likely be changed" } };

            this.userService = new Mock<IUserService>();
            this.postService = new Mock<IPostService>();

            this.config = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();
        }
    }
}