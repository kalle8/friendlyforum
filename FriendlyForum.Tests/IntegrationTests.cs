using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using FriendlyForum.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;

namespace FriendlyForum.Tests
{
    public class ForumIntegrationTests : IClassFixture<WebApplicationFactory<FriendlyForum.Startup>>
    {
        private HttpClient _client { get; }
        private readonly ITestOutputHelper _output;

        public ForumIntegrationTests(WebApplicationFactory<FriendlyForum.Startup> fixture, ITestOutputHelper output)
        {
            this._client = fixture.CreateClient();
            this._client.DefaultRequestHeaders.Clear();
            this._client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            this._output = output;

        }

        [Fact]
        public async Task Requires_Auth()
        {
            var response = await _client.GetAsync("/api/v1/posts/");
            this._output.WriteLine("Expecting a 401 status");
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Can_login()
        {
            var loginData = new Dictionary<string, string>() {
                {"Name", "Kalle"}
            };
            var response = await this._client.PostAsync("/api/v1/auth/authenticate?name=Kalle", null);
            this._output.WriteLine("Authenticating against API");

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var authData = JsonConvert.DeserializeObject<AuthData>(await response.Content.ReadAsStringAsync());

            authData.User.Name.Should().Be("Kalle");
        }

        [Fact]
        public async Task Can_auth_and_CRUD_post()
        {
            var loginData = new Dictionary<string, string>() {
                {"Name", "Kalle"}
            };
            // LOGIN
            var response = await this._client.PostAsync("/api/v1/auth/authenticate?name=Kalle", null);
            var authData = JsonConvert.DeserializeObject<AuthData>(await response.Content.ReadAsStringAsync());
            this._client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authData.Token);
            this._output.WriteLine("Authenticating against API ");

            // C - POST
            var postResponse = await this._client.PostAsync("/api/v1/posts/",
                                new StringContent(JsonConvert.SerializeObject(new CreatePostDto("First post!", "This is exciting")),
                                Encoding.UTF8, "application/json"));
            this._output.WriteLine("Creating a post");

            postResponse.StatusCode.Should().Be(HttpStatusCode.Created);

            var originalPost = JsonConvert.DeserializeObject<ForumPost>(await postResponse.Content.ReadAsStringAsync());

            originalPost.Title.Should().Be("First post!");

            // R - GET created post
            var postGetResponse = await this._client.GetAsync($"/api/v1/posts/{originalPost.Id}");
            this._output.WriteLine("Fetching post");

            postGetResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            var post = JsonConvert.DeserializeObject<ForumPost>(await postGetResponse.Content.ReadAsStringAsync());
            // Should still be the same title
            post.Title.Should().Be("First post!");

            // U - PUT 
            var postPutResponse = await this._client.PutAsync($"/api/v1/posts/{post.Id}",
                                new StringContent(JsonConvert.SerializeObject(new CreatePostDto("Second post?", "This is not as exciting")),
                                Encoding.UTF8, "application/json"));
            this._output.WriteLine("Updating post");

            postPutResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            post = JsonConvert.DeserializeObject<ForumPost>(await postPutResponse.Content.ReadAsStringAsync());


            post.Title.Should().Be("Second post?");
            post.Updated.Should().NotBe(originalPost.Updated);
            post.Edited.Should().Be(true);

            // D - DELETE our post
            var postDeleteResponse = await this._client.DeleteAsync($"/api/v1/posts/{post.Id}");
            this._output.WriteLine("Deleting post");

            var ourDeletedPost = await this._client.GetAsync($"/api/v1/posts/{post.Id}");
            ourDeletedPost.StatusCode.Should().Be(HttpStatusCode.NotFound);
            postDeleteResponse.StatusCode.Should().Be(HttpStatusCode.OK);

        }
    }
}
