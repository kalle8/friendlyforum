using System;

namespace FriendlyForum.Contracts
{
    public class PermissionDeniedException : Exception
    {
        public PermissionDeniedException(string message) : base(message)
        {

        }
    }
}