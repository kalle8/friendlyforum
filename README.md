# Friendly Forum

Friendly Forum is a simple service based on trust. All you need to start posting about your life's adventures is a name. 

To build and run our service clone this repo and run the following

```sh
$ dotnet run --project FriendlyForum.Web
```

All APIs require you to authenticate and save a JWT Token.  

`POST https://localhost:5001/api/v1/auth/authenticate?name=<your name>`

You'll get back a JSON object with a User and a token. 
```json
{
   "user": {
       "id": "guid id",
       "name": "<your name>",
       ...
   },
   "token": "JWT Token"
}
```

The following API is available once you have created a token 

To create a new post 

`POST https://localhost:5001/api/v1/posts/`
```json
{
    "title": "The title of your post",
    "post": "The post body"
}
```

To fetch a post 

`GET https://localhost:5001/api/v1/posts/{id}`

Return value
```json
{
    "id": "guid id",
    "title": "The title of your post",
    "post": "The post body",
    "created": "2021-06-17 23:00",
    "updated": "2021-06-17 23:00",
    "edited": false,
    "user": {
        "name": "Author",
        ...
    }
}
```

To update a post 

`PUT https://localhost:5001/api/v1/posts/{id}`
```json
{
    "title": "The title of your post",
    "post": "The post body",
}
```

To delete a post 

`DELETE https://localhost:5001/api/v1/posts/{id}`


To get all available poistings

`GET https://localhost:5001/api/v1/posts/{id}`

Return value
```json
[{
    "id": "guid id",
    "title": "The title of your post",
    "post": "The post body",
    "created": "2021-06-17 23:00",
    "updated": "2021-06-17 23:10",
    "edited": true,
    "user": {
        "name": "Author",
        ...
    }
}]
```

To run the accompanying tests
```sh
$ dotnet test
```

![alt text](assets/api.png "Api Overview")


