using System;

namespace FriendlyForum.Models
{
    public class ErrorDto
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public DateTime Occurred { get; set; }
        public ErrorDto(Exception exception)
        {
            this.Type = exception.GetType().Name;
            this.Message = exception.Message;
            this.Occurred = DateTime.UtcNow;
        }
    }

}