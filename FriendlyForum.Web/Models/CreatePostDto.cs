using System.ComponentModel.DataAnnotations;

namespace FriendlyForum.Models
{
    public class CreatePostDto
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }

        public CreatePostDto(string title, string body)
        {
            this.Title = title;
            this.Body = body;
        }
    }
}