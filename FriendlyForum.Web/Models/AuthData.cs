namespace FriendlyForum.Models
{
    public class AuthData
    {
        public User User { get; set; }
        public string Token { get; set; }
    }
}