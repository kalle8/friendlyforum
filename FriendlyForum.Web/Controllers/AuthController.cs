using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FriendlyForum.Helpers;
using FriendlyForum.Models;
using FriendlyForum.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace FriendlyForum.Controllers
{
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IUserService _userService;
        private ILogger<AuthController> _logger;
        private readonly IConfiguration _configuration;


        public AuthController(IUserService userService, ILogger<AuthController> logger, IConfiguration config)
        {
            this._userService = userService;
            this._logger = logger;
            this._configuration = config;
        }


        /// <summary>
        /// Simple auth 
        /// Will create a new user or re-auth already existing user 
        /// </summary>
        /// <param name="name">User must provide a name to authenticate</param>
        /// <returns>OK Result with AuthData (Token, User) values</returns>

        [AllowAnonymous]
        [HttpPost, Route("api/v1/auth/authenticate")]
        public async Task<ActionResult<AuthData>> AuthenticateAsync(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return BadRequest("Missing name parameter");
            }

            try
            {
                var user = await this._userService.AuthenticateAsync(name);
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(this._configuration["Secret"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);

                this._logger.LogInformation("User {0} ({1}) authenticated", user.Name, user.Id);

                return Ok(new AuthData() { User = user, Token = tokenString });
            }
            catch (Exception ex)
            {
                this._logger.LogError("User subsystem threw exception {0}", ex.Message);
                return this.HandleException(ex);
            }
        }
    }
}