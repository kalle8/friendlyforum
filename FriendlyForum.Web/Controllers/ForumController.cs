using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FriendlyForum.Contracts;
using FriendlyForum.Helpers;
using FriendlyForum.Models;
using FriendlyForum.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FriendlyForum.Controllers
{
    [Authorize]
    [ApiController]
    public class ForumController : ControllerBase
    {
        private readonly ILogger<ForumController> _logger;
        private readonly IPostService _postService;

        public Guid UserId { get; set; } = Guid.Empty;

        public ForumController(ILogger<ForumController> logger, IPostService postService)
        {
            this._logger = logger;
            this._postService = postService;
        }

        /// <summary>
        /// Get all posts
        /// </summary>
        /// <returns>OK Result with list/array of all posts</returns>

        [HttpGet]
        [Route("api/v1/posts/")]
        public IActionResult GetPosts()
        {
            try
            {
                var posts = this._postService.GetPosts();
                return Ok(posts);
            }
            catch (Exception ex)
            {
                return this.HandleException(ex);
            }
        }

        /// <summary>GET a single post by ID</summary>
        /// <param name="id">ID of post</param>
        /// <returns>OK Result with Post</returns>

        [HttpGet]
        [Route("api/v1/posts/{id}")]
        public async Task<IActionResult> GetPost(Guid id)
        {
            try
            {
                var post = await this._postService.GetPost(id);
                return Ok(post);
            }
            catch (Exception ex)
            {
                return this.HandleException(ex);
            }
        }


        /// <summary>Create a new post</summary>
        /// <param name="postDto">A <see cref="CreatePostDto"/> object</param>
        /// <returns>201 Created with location, id and the new post</returns>

        [HttpPost]
        [Route("api/v1/posts/")]
        public async Task<IActionResult> CreatePost(CreatePostDto postDto)
        {
            if (!ModelState.IsValid)
            {
                this._logger.LogWarning($"User {this.GetUserId()} tried to lazily create a new post.", null);
                return BadRequest("Missing key components of a post");
            }
            try
            {
                var post = await this._postService.CreatePost(this.GetUserId(), postDto.Title, postDto.Body);
                return CreatedAtAction(nameof(GetPost), new { Id = post.Id }, post);
            }
            catch (Exception ex)
            {
                return this.HandleException(ex);
            }

        }

        /// <summary>Delete a post by ID</summary>
        /// <param name="id">Id of post</param>
        /// <returns>Empty OK Result</returns>
        [HttpDelete]
        [Route("api/v1/posts/{id}")]
        public async Task<IActionResult> DeletePost(Guid id)
        {
            try
            {
                await this._postService.DeletePost(this.GetUserId(), id);
                return Ok();
            }
            catch (Exception ex)
            {
                return this.HandleException(ex);
            }
        }

        /// <summary>Update a post by id</summary>
        /// <param name="Id">Id of post</param>
        /// <param name="postUpdate">A <see cref="CreatePostDto"/> object</param>
        [HttpPut]
        [Route("api/v1/posts/{id}")]
        public async Task<IActionResult> UpdatePost(Guid id, CreatePostDto postUpdate)
        {
            try
            {
                var updatedPost = await this._postService.UpdatePost(this.GetUserId(), id, postUpdate.Title, postUpdate.Body);
                return Ok(updatedPost);
            }
            catch (Exception ex)
            {
                return this.HandleException(ex);
            }
        }
        private Guid GetUserId()
        {
            if (this.UserId == Guid.Empty)
            {
                if (User?.Identity?.Name == null)
                {
                    throw new NotAuthorizedException("Authorization not available");
                }

                this.UserId = Guid.Parse(User.Identity.Name);
            }

            return this.UserId;
        }

    }
}