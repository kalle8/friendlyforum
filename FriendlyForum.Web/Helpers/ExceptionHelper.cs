using System;
using FriendlyForum.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FriendlyForum.Helpers
{
    public static class ControllerExtensions
    {
        public static ActionResult HandleException(this ControllerBase controller, Exception ex)
        {
            return ex switch
            {
                BadRequestException => controller.BadRequest(ex.Message),
                PermissionDeniedException => controller.Forbid(ex.Message),
                NotFoundException => controller.NotFound(ex.Message),
                NotAuthorizedException => controller.Unauthorized(ex.Message),
                _ => controller.StatusCode(500, ex.Message)
            };
        }

    }
}