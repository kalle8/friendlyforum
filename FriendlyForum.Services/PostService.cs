using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FriendlyForum.Contracts;
using FriendlyForum.Models;
using Microsoft.Extensions.Logging;

namespace FriendlyForum.Services
{

    public class PostService : IPostService
    {
        private readonly ILogger<PostService> _logger;
        private readonly FriendlyContext _context;

        public PostService(ILogger<PostService> logger, FriendlyContext context)
        {
            this._logger = logger;
            this._context = context;
        }

        public async Task<ForumPost> CreatePost(Guid userId, string title, string body)
        {
            var newPost = new ForumPost(userId, title, body);
            await this._context.ForumPosts.AddAsync(newPost);
            await this._context.SaveChangesAsync();

            this._logger.LogInformation($"User {userId} created a new post '{newPost.Title}' ({newPost.Id}).", null);

            return newPost;
        }

        public async Task<ForumPost> GetPost(Guid id)
        {
            var post = await this._context.ForumPosts.FindAsync(id);
            if (post == null)
            {
                throw new NotFoundException($"Post with '{id}' not found");
            }
            return post;
        }

        public List<ForumPost> GetPosts()
        {
            return this._context.ForumPosts.ToList() ?? new List<ForumPost>();
        }

        public async Task<ForumPost> UpdatePost(Guid userId, Guid id, string title, string body)
        {
            var post = await this.GetPost(id);

            if (userId != post.UserId)
            {
                throw new PermissionDeniedException($"Not allowed to update post '{id}'");
            }

            post.Update(title, body);
            await this._context.SaveChangesAsync();

            return post;
        }


        public async Task DeletePost(Guid userId, Guid id)
        {
            var post = await this.GetPost(id);

            if (userId != post.UserId)
            {
                throw new PermissionDeniedException($"Not allowed to update post '{id}'");
            }

            this._context.Remove(post);
            await this._context.SaveChangesAsync();
        }
    }
}