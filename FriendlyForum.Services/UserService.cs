using System;
using System.Linq;
using System.Threading.Tasks;
using FriendlyForum.Models;
using Microsoft.Extensions.Logging;

namespace FriendlyForum.Services
{

    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly FriendlyContext _context;

        public UserService(ILogger<UserService> logger, FriendlyContext context)
        {
            this._logger = logger;
            this._context = context;
        }

        public async Task<User> AuthenticateAsync(string name)
        {
            var user = this._context.Users.SingleOrDefault(x => x.Name == name);

            if (user == null)
            {
                user = await this.CreateAsync(name);
            }

            return user;
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            return await this._context.Users.FindAsync(userId);
        }

        private async Task<User> CreateAsync(string name)
        {
            var user = new User(name);
            await this._context.AddAsync(user);
            await this._context.SaveChangesAsync();
            return user;
        }
    }
}