using System;
using System.Threading.Tasks;
using FriendlyForum.Models;

namespace FriendlyForum.Services
{
    public interface IUserService
    {
        Task<User> AuthenticateAsync(string name);
        Task<User> GetByIdAsync(Guid userId);
    }
}