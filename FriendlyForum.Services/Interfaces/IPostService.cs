using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FriendlyForum.Models;

namespace FriendlyForum.Services
{
    public interface IPostService
    {
        Task<ForumPost> CreatePost(Guid userId, string title, string post);
        Task<ForumPost> GetPost(Guid id);
        List<ForumPost> GetPosts();
        Task<ForumPost> UpdatePost(Guid userId, Guid id, string title, string body);
        Task DeletePost(Guid userId, Guid id);
    }
}